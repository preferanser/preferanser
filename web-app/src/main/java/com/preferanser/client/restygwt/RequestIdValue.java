package com.preferanser.client.restygwt;

public enum RequestIdValue {

    GET_CURRENT_USER,
    SAVE_DEAL,
    GET_DEAL,
    DELETE_DEAL,
    LOAD_DEALS,

}
